<?define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);?>

<?


$descriptor = fopen('act/file1050.txt', 'r');
if ($descriptor) {
    while (($string = fgets($descriptor)) !== false) {
        $arr = explode("|", $string);
        drupal_set_time_limit(600);
        addNode($arr[0], $arr[1], $arr[2], $arr[3], trim($arr[4]));
        
        
    }
    fclose($descriptor);

} else {
    echo 'Невозможно открыть указанный файл';
}

function addNode($num, $date, $title, $body, $filePath){
$title = str_replace("\\", "",$title);
$body = str_replace("\\", "",$body);
$title = strip_tags($title);
$title = substr($title, 0, 180);
$title = rtrim($title, "!,.-");
$title = substr($title, 0, strrpos($title, ' '))."...";



    
$node = new stdClass();
$node->type = 'mun_prav_act';
node_object_prepare($node);
$node->language = 'und';
$node->title = "ПОСТАНОВЛЕНИЕ от ".date('d.m.Y',$date)." г. №".$num.":".$title;
$node->body['und'][0]['value'] = $body;
$node->body['und'][0]['format'] = "full_html";
$node->field_number['und'][0]['value']= $num;
$node->field_mun_date['und'][0]['value']= date('Y-m-d',$date);
$node->created = $date;


$node->status = 1;
$node->promote = 1;
$filePath = "http://www.admmaloyaroslavec.ru/".$filePath;
if($filePath){
    try {
        $filename = basename($filePath); 
        //$extension = end(explode(".", $filename));
        
        $destination='public://'.$filename;
        
        $data = drupal_http_request($filePath);
        if($data->code==200){ 
            
            //Сохраняем файл 
            $file = file_save_data($data->data, $destination);
            if ($file) {
                $file->display = 1;
                $node->field_file['und'][] = (array)$file;
            }
        }
        
    } catch (Exception $e) {
        echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
    }
}
node_save($node);
echo "Добавлен ".$num. "  ".$title."<br>";
}?>