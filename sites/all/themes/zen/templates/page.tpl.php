﻿<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>
<link rel="stylesheet" type="text/css" href="https://nst1.gismeteo.ru/assets/flat-ui/legacy/css/informer.min.css">
    <div id="gsInformerID-4M83S80gSJLI0w" class="gsInformer" style="width:240px;height:0px!important;display;none!important;">
        <div class="gsIContent">
            <div id="cityLink">
                                    <a href="https://www.gismeteo.ru/weather-maloyaroslavets-4366/" target="_blank">Погода в Малоярославце</a>
            </div>
            <div class="gsLinks">
            </div>
        </div>
    </div>
<script async src="https://www.gismeteo.ru/api/informer/getinformer/?hash=4M83S80gSJLI0w" type="text/javascript"></script>


<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'ru', includedLanguages: 'en,ru,zh-CN', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<div id="scrollup">
    <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
</div>
<nav class="media-menu">
    <div class="media-close">
        <i class="fa fa-times" aria-hidden="true"></i>
    </div>

    <div class="container-pad-0">

    </div>
</nav>
<div class="wrapper">

    <div class="top-bar">
            <div class="container">
                <?if(render($page['top_menu'])):?>
                    <div class="top-bar-menu">
                        <div class="media-logo" style="color: rgb(0, 0, 0);">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </div>
                        <?php print render($page['top_menu']); ?>
                    </div>
                <?endif;?>
                <p>
                    <a class="change-lang ru" data-lang="ru">
                        <img alt="" src="http://malii.avtookei.com/sites/all/themes/zen/assets/img/FlagRussia.png">
                    </a> 
                    <a class="change-lang en" data-lang="en"> 
                        <img alt="" src="http://malii.avtookei.com/sites/all/themes/zen/assets/img/FlagEngland.png">
                    </a> 
                    <a class="change-lang china" data-lang="zh-CN"> 
                        <img alt="" src="http://malii.avtookei.com/sites/all/themes/zen/assets/img/FlagChina.png">
                    </a>
                    </p>
            </div>
        </div>

    <header>
        <div class="header">
            <div class="container">
                <div class="wrapper-first-second-block">
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo" class="first-block">
                        <div style="background-image: url('<?php print $logo; ?>')" class="wrapper-image-logo"></div>
                        <div class="wrapper-first-block-title">
                            <h1>Малоярославец</h1>

                            <p>ГОРОД ВОИНСКОЙ СЛАВЫ</p>
                        </div>
                    </a>
                    <div class="second-block">
                        <div class="header__region region region-header">
                            <div class="block block-block first last odd" id="block-block-2">
                                <div class="wrapper-date"><!--Добавил-->
                                    <div style="background-image: url('http://malii.avtookei.com/sites/all/themes/zen/assets/img/IconClock.png')" class="wrapper-icon-clock"></div><!--Добавил-->
                                    <div class="wrapper-time"><!--Добавил-->
                                        <p class="time">17:07</p><!--Добавил-->
                                        <p class="date">28.06.2018</p><!--Добавил-->
                                    </div>
                                </div>
                                <div class="wrapper-temperature"><!--Добавил-->
                                    <div style="background-image: url('http://malii.avtookei.com/sites/all/themes/zen/assets/img/IconCloud.svg')" class="wrapper-icon-cloud"></div><!--Добавил-->
                                    <div class="wrapper-temp"><!--Добавил-->
                                        <p class="name">Малоярославец</p><!--Добавил-->
                                        <p class="temp">--</p><!--Добавил-->
                                    </div>
                                </div>
                                <!--<p>здесь должен быть скрипт погоды</p>-->
                            </div>
                        </div>
                        <?if(render($page['head_right'])):?>
                            <?php print render($page['head_right']); ?>
                        <?endif;?>
                        
                    </div>
                </div>
                <div class="navigation">
                    <?php print render($page['navigation']); ?>
                </div>
            </div>
        </div>
    </header>
    
   <?php if(drupal_is_front_page()):?>
    <div class="section" id="news-slider">
        <div class="container">
        <?php print render($page['top_banner']); ?>  
        </div>
    </div>
    <div class="section" id="main-columns">
        <div class="container"><!--Добавил-->
    
            <div class="main-container">
                <div class="column-sidebar column col-4">
                    <?php print render($page['first_column']); ?> 
                </div><!--Добавил класс column-first-->
                <div class="column-second column-popular-news column col-4">
                    <?php print render($page['second_column']); ?>
                </div><!--Добавил класс column-second-->
                <div class="column-oficial-inform column col-4">
                    <?php print render($page['third_column']); ?>
                </div><!--Добавил класс column-third-->
                <div class="column-img-news column col-4">
                   <?php print render($page['four_column']); ?>
                </div>
    
            </div>
        </div>
    </div>
    
    <div class="section" id="photogallery">
        <?php print render($page['photogallery']); ?>  
    </div>
    <div class="section" id="maps">
        <?php print render($page['maps']); ?>  
    </div>
    
    <?php else:?>
        <div class="container">
            <?php //print $breadcrumb; ?>
            <div class="display-flex">
                <?php if (render($page['sidebar_first'])): ?>
                  <aside class="left-sidebar" role="complementary">
                    <?php print render($page['sidebar_first']); ?>
                  </aside>
                <?php endif; ?>
                <div class="section main-content <?=$node ? $node->type : ""?>" role="main">
                
                  <?php print render($page['highlighted']); ?>
                  
                  <?php print render($title_prefix); ?>
                  <?php if ($title): ?>
                    <h1><?php print $title; ?></h1>
                  <?php endif; ?>
                  <?php print render($title_suffix); ?>
                  <?php print $messages; ?>
                  <?php print render($tabs); ?>
                  <?php print render($page['help']); ?>
                  <?php if ($action_links): ?>
                    <ul class="action-links"><?php print render($action_links); ?></ul>
                  <?php endif; ?>
                  <?php print render($page['content']); ?>
                  <?php print $feed_icons; ?>
               
                </div>
                <?php if (render($page['sidebar_second'])): ?>
                  <aside class="right-sidebar column-oficial-inform column-popular-news" role="complementary">
                    <?php print render($page['sidebar_second']); ?>
                  </aside>
                <?php endif; ?>
            </div>
        </div>
    <?php endif;?>
   <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="first-block">
                    <?php print render($page['footer_first_block']); ?>              
                </div>
                <div class="second-block">
                    <?php print render($page['footer_second_block']); ?>
                </div>
            </div>
            <div class="footer-navigation">
                <?php print render($page['footer_menu']); ?>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <?php print render($page['footer_bottom']); ?>
            </div>
        </div>
    </footer>
</div>
<div class="parent">
    <div id="block">
        <div class="close-menu">x</div>

    </div>
</div>
<div id="lean_overlay"></div>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script async src="https://www.gismeteo.ru/api/informer/getinformer/?hash=Ju18KLSFr8F85v" type="text/javascript"></script>



