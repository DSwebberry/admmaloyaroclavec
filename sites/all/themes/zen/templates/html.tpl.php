<?php
/**
 * @file
 * Returns the HTML for the basic html structure of a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728208
 */
?><!DOCTYPE html>
<html <?php print $html_attributes . $rdf_namespaces; ?>>
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>

  <?php if ($default_mobile_metatags): ?>
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
  <?php endif; ?>

  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php if ($add_html5_shim): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_zen; ?>/js/html5shiv.min.js"></script>
    <![endif]-->
  <?php endif; ?>
  
  
    <link href="<?php print $base_path . $path_to_zen; ?>/assets/css/FontAwesoome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php print $base_path . $path_to_zen; ?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php print $base_path . $path_to_zen; ?>/assets/css/media.css" rel="stylesheet">
    <link href="<?php print $base_path . $path_to_zen; ?>/assets/css/fonts.css" rel="stylesheet">
    
      
    <script src="<?php print $base_path . $path_to_zen; ?>/assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php print $base_path . $path_to_zen; ?>/assets/js/app.js"></script>
    <script src="<?php print $base_path . $path_to_zen; ?>/assets/js/jquery.bxslider.min.js"></script>
    <script src="<?php print $base_path . $path_to_zen; ?>/assets/js/jquery.nicescroll.min.js"></script>
  
  <!--print $base_path . $path_to_zen = /sites/all/themes/zen/
  <!--$base_path . $path_to_zen/assets/js/script.js-->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
    <div id="stage"> 
    <p id="spinner"></p>
</div>
  <?php if ($skip_link_text && $skip_link_anchor): ?>
    <p class="skip-link__wrapper">
      <a href="#<?php print $skip_link_anchor; ?>" class="skip-link visually-hidden visually-hidden--focusable" id="skip-link"><?php print $skip_link_text; ?></a>
    </p>
  <?php endif; ?>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
