

$(document).ready(function ($) {
    $('select.date-month option[value="2"]').text("фев");
    $("#main-columns .column-popular-news .view-popular-news-block-on-the-page ").append('<a href="/news" class="reed-more">Все новости</a>')
    $("#main-columns .column-oficial-inform .view-block-official-information-on-the-home").append('<a href="/oficialno" class="reed-more">Все материалы раздела</a>')
    
    
    $(".front .official-information-item").each(function(){
        var href = $(this).find(".views-field-title a").attr("href");
        $(this).append("<a class='more' href='"+href+"'>Подробнее...</a>")
    })
    
    var visual_impaired = parseInt(localStorage.getItem("visually_impaired"));

    if(visual_impaired){
        localStorage.setItem("visually_impaired", 1);
	    $('body').css('zoom','135%'); /* Webkit browsers */
        $('body').css('zoom','1.35'); /* Other non-webkit browsers */
    }
   
        
    $(document).on("click", ".visually-impaired", function(){
        $("#stage").toggleClass("hidden");
        var visual_impaired = parseInt(localStorage.getItem("visually_impaired"));
       
        if(visual_impaired){
            localStorage.setItem("visually_impaired", 0);
    	    $('body').css('zoom','100%'); /* Webkit browsers */
            $('body').css('zoom','1'); /* Other non-webkit browsers */
        }
        else{
            localStorage.setItem("visually_impaired", 1);
    	    $('body').css('zoom','135%'); /* Webkit browsers */
            $('body').css('zoom','1.35'); /* Other non-webkit browsers */
        }
        $("#stage").toggleClass("hidden");
        
    });
    
    
    $(".form-item-search-block-form").append("<span class='js-search'></span>")
    $("#stage").addClass("hidden");
    setTimeout(function(){
        var href = $(".gsInformer .gsWeatherIcon  img").attr("src")
        $(".wrapper-temperature .wrapper-temp .temp").text($('.gsIContent .gsTemp > span').text())
        $(".wrapper-temperature .wrapper-icon-cloud").css("background-image","url('"+href+"')")
    }, 1500)
    
    var dated = new Date();
    
    function triggerHtmlEvent(element, eventName) {
        var event;
        if(document.createEvent) {
            event = document.createEvent('HTMLEvents');
            event.initEvent(eventName, true, true);
            element.dispatchEvent(event);
        } else {
            event = document.createEventObject();
            event.eventType = eventName;
            element.fireEvent('on' + event.eventType, event);
        } 
    }
    
    $(document).on("click", ".change-lang", function(){
        var lang = $(this).data("lang");
        var $select = $(document).find(".goog-te-combo")
        $select.val(lang);
        triggerHtmlEvent($select[0], 'change');
    })
    
    $(document).on("click", ".js-search", function(){
        $(this).closest(".container-inline").find(".form-actions [type='submit']").trigger("click");
    });
    
// час в текущей временной зоне
    console.log( dated.getMonth() );



    //Время
    var clock = $('.wrapper-time .time');
    var date = $('.wrapper-time .date');
    function hexoclock(){
        var time = new Date();
        var h =time.getHours().toString();
        var m = time.getMinutes().toString();

        var mon = time.getMonth()+1;
        mon = mon.toString();
        var d = time.getDate().toString();
        var y = time.getFullYear().toString();
        //var s = time.getSeconds().toString();

        if(h.length<2){
            h='0'+h
        }
        if(m.length<2){
            m='0'+m
        }

        if(d.length<2){
            d='0'+d
        }
        if(mon.length<2){
            mon='0'+mon
        }
        //if(s.length<2){
        //    s='0'+s
        //}
        var clockString = h+':'+m;
        var dateString = d+'.'+mon+'.'+y;

        $(clock).html(clockString);
        $(date).html(dateString);


    }
    setInterval(hexoclock,60000);
    hexoclock();

    //Скролл в начало страницы

    //jQuery(function(f){
    //    var element = f('#scrollup');
    //    f(window).scroll(function(){
    //        element['fade'+ (f(this).scrollTop() > 200 ? 'In': 'Out')](500);
    //    });
    //});


    var element = $('#scrollup');
    $(window).scroll(function(){
        //if ($(element).scrollTop() > 200) {
        //    $(element).css('opacity', 1)
        //}
        //element['fade'+ ($(this).scrollTop() > 200 ? 'In': 'Out' )](500);

        if($(this).scrollTop() > 200){
            element.css('display','flex');
            element.fadeIn(500);
        }
        else{
            element.fadeOut(500)
        }

    });



    $(element).click(function(event){
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0}, 500);
    });


    /*-----------МЕНЮ-------------*/

    /*Вызов Меню*/
    $('.media-logo').on("click",function(e){
        $('.wrapper').css('display','none');
        $('body').addClass('bg-color-body');
        $('.media-menu').css("display","block");
        e.preventDefault();
    });
    /*Закрыть меню*/
    $(".media-close").click(function(){
        $('body').removeClass('bg-color-body');
        $('.media-menu').css("display","none");
        $('.wrapper').css('display','block');

    });

    $('body').on('click','.media-menu ul#superfish-1 li',function(){
        $(this).find('ul').slideToggle(500,'swing')

        //alert("Привет")
    });


    //Цвет меняется иконки



    $(document).scroll(function() {

        var offset_top = $('.footer').offset().top;

        if ($(this).scrollTop() >= offset_top) {
            // создаем эффекты и анимацию
            $(".media-logo").css('color','#fff')

        }else{
            $(".media-logo").css('color','#000')
        }
    });

   // Слайдер по клику на область с заголовком новости


    var item = $('.slide-news-block .item-news');
    var item_2 = $('.wrapper-news-slide-img .slide-image-block');
    var item_2_active = $('.wrapper-news-slide-img .slide-image-block.active');


    //Формирования треугольного указателя при загрущке страницы для активного слайда
    //$(item).each(function(){
    //    if($(this).hasClass('active')){
    //        var height_elem = $(this).outerHeight();
    //        console.log(height_elem);
    //        $(this).find('.wrapper-before-item-news').css({
    //            //"display": "block",
    //            "borderTopWidth": height_elem/2-1,
    //            "borderBottomWidth": height_elem/2-1
    //        })
    //    }
    //});


    function active_item_news(){
        var height_elem = $('.slide-news-block .item-news.active').outerHeight();
        console.log(height_elem);
        $('.slide-news-block .item-news.active').find('.wrapper-before-item-news').css({
            //"display": "block",
            "borderTopWidth": height_elem/2-1,
            "borderBottomWidth": height_elem/2-1
        });
    };


    setTimeout(active_item_news,200);
    $(window).on('resize', function(){
        setTimeout(active_item_news,200);
    });



    //Функция для фомирования указателя активной новости при клике на область с заголовком новости

    function show_before_item (el_click){ //в качестве параметра, будет элемент, на который кликнули
        $(item).removeClass('active');
        $('.wrapper-before-item-news').css('display','none');
        $(el_click).addClass('active');
        var height_item = $(el_click).outerHeight();


        $(el_click).find('.wrapper-before-item-news').css({
            "display": "block",
            "borderTopWidth": height_item/2-1,
            "borderBottomWidth": height_item/2-1

        })
    }

//    Функция для формирования активного слайда

    function active_new(el_click){//в качестве параметра, будет элемент, на который кликнули
        var data_attr = $(el_click).attr('data-news_slide');
        $('.wrapper-news-slide-img .slide-image-block').removeClass('active');
        $('.wrapper-news-slide-img .slide-image-block[data-news_slide='+data_attr+']').addClass('active');
    }

    //Вызов функций при клике на область с заголовком новости. В функцию передаю элемент, на который кликнул

    $(item).on('click',function(){
        show_before_item (this);
        active_new(this);


    });

    //Слайдер по клику на стрелки

    var massive = [];

    $(item_2).each(function(){
        massive.push(parseInt($(this).attr('data-number_slide')));
    });

    var prev_button = $('.item-slide-arrow .prev-button');
    var next_button = $('.item-slide-arrow .next-button');

    $(prev_button).on('click',function(){

        var active_attr_slide = $('.wrapper-news-slide-img .slide-image-block.active').index();


        $('.wrapper-news-slide-img .slide-image-block[data-number_slide='+active_attr_slide+']').removeClass('active');

        if(active_attr_slide==massive.length-1){

            $('.wrapper-news-slide-img .slide-image-block[data-number_slide='+(massive[0])+']').addClass('active');
            active_before_item_next (massive[0])
        }
        else{
            active_attr_slide+=1;
            $('.wrapper-news-slide-img .slide-image-block[data-number_slide='+active_attr_slide+']').addClass('active');
            active_before_item_next (active_attr_slide)
        }


    });

    //Функция для активации области с заголовком, в качестве параметра номер элемента, получаемый через index
    //вызывается при клике на стрелуку

    function active_before_item_next (el_click){
        $('.slide-news-block .item-news').removeClass('active');
        $('.slide-news-block .item-news[data-number_slide='+el_click+']').addClass('active');

        $('.wrapper-before-item-news').css('display','none');
        var height_item = $('.slide-news-block .item-news[data-number_slide='+el_click+']').outerHeight();
        $('.slide-news-block .item-news[data-number_slide='+el_click+']').find('.wrapper-before-item-news').css({
            "display": "block",
            "borderTopWidth": height_item/2-1,
            "borderBottomWidth": height_item/2-1

        })
    };


    $(next_button).on('click',function(){

        var active_attr_slide = $('.wrapper-news-slide-img .slide-image-block.active').index();

        $('.wrapper-news-slide-img .slide-image-block[data-number_slide='+active_attr_slide+']').removeClass('active');

        if(active_attr_slide==massive[0]){

            $('.wrapper-news-slide-img .slide-image-block[data-number_slide='+(massive.length-1)+']').addClass('active');
            active_before_item_next (massive.length-1)
        }
        else{
            active_attr_slide-=1;
            $('.wrapper-news-slide-img .slide-image-block[data-number_slide='+active_attr_slide+']').addClass('active');
            active_before_item_next (active_attr_slide)
        }

    });


    //Класс slidedawn

    function Slidedawn(obg){
        this.elem = obg.title;
        this.elem_2 = obg.elem_hide;
        this.widt = obg.window_width;

        var that = this;//описано выше зачем, чтобы this был не в контексте метода, а класса
        this.method=function(){
            $(this.elem).on('click',function(){
                if ($(window).width()<=that.widt){
                    $(this).siblings(that.elem_2).slideToggle(500,'swing')
                }

            });

            //$(this.elem).on('click',function(){
            //    if ($(this).siblings(that.elem_2).css('display') == 'none'){
            //        $(this).siblings(that.elem_2).slideToggle(500,'swing')
            //    }
            //
            //});




        };
        that.method()
    }

    new Slidedawn({
        title:".first-block h2",
        elem_hide:".first-block p",
        window_width:340
    });

    new Slidedawn({
        title:".second-block h2",
        elem_hide:".second-block ul.menu",
        window_width:340
    });



    new Slidedawn({
        title:".column-popular-news h2",
        elem_hide:".main-container .column-popular-news .view",
        window_width:625
    });

    new Slidedawn({
        title:".column-oficial-inform h2",
        elem_hide:".main-container .column-oficial-inform .view",
        window_width:625
    });

    new Slidedawn({
        title:".column-third h2",
        elem_hide:".main-container .column-third .view",
        window_width:520
    });

//    Слайдер Фотогаллереи

//        $('.bxslider').bxSlider({
//            mode:"horizontal",
//            slideMargin:10,
//            adaptiveHeight:true,
//            controls: false,
//            pager: true,
//            nextText:"",
//            prevText:"",
//            minSlides:1,
//            maxSlides:5,
//            slideWidth:213,
//            onSliderLoad: function(){
//                $('.bx-viewport').prepend('<div class="try"></div>')
//                $('.bx-viewport').prepend('<div class="try-2"></div>')
//
//            }
////
//        });

    var maxSlides,
    width = $(window).width();

        if (width < 1140 && width > 930) {
            maxSlides = 4;
        }
        else if (width < 930 && width > 715) {
            maxSlides = 3;
        }
        else if (width < 715 && width > 490) {
            maxSlides = 2;
        }
        else if (width < 490) {
            maxSlides = 1;
        }
        else {
            maxSlides = 5;
        }



    var myslider = $('#photogallery .view-fromt-page-gallery .view-content').bxSlider({
        mode:"horizontal",
        slideMargin:10,
        adaptiveHeight:true,
        controls: false,
        pager: true,
        nextText:"",
        prevText:"",
        //minSlides:1,
        maxSlides:maxSlides,
        slideWidth:213,
        onSliderLoad: function(){
           // $('.bx-viewport').prepend('<div class="try"></div>')
            //$('.bx-viewport').prepend('<div class="try-2"></div>')

        }
});



//Клон меню

    $(".navigation ul#superfish-1").clone(true).appendTo(".container-pad-0");

    $("#photogallery .bxslider .item-slider a").click(function(event) {
        event.preventDefault();


    });

//    Красивый скролл таблиц

    $(".contacts article.node-page, .table .block-views").niceScroll({
        cursorcolor: "#cac7c7", //цвет ползунка
        autohidemode: false,
        cursorwidth: 6,  //ширина ползунка
        background: "transparent" //цвет линии сролла
    });


//    Работа с элементами на странице Гоостиницы и Рестораны. Добавляю кнопки набор номера и карта


    var new_elem ="<div class='buttons-call-to-action'>" +
        "<a href='tel:' class='buttons-c-t-a buttons-c-t-a-phone'><div></div></a>" +
        "<div data-adress='' class='buttons-c-t-a buttons-c-t-a-map'><div></div></div>" +
        "</div>";

    var new_elem_2="<div class='wrapper-t'></div>";

    var number;

    $('.hotels-and-services .services-item').each(function(index){
        $(new_elem_2).insertAfter($(this).find('.views-field-field-image'));

        $(this).find('div.views-field').each(function(){
           $(this).not('.views-field-field-image').appendTo($(this).parents('.hotels-and-services .services-item').find('.wrapper-t'));
        });


        //$(this).find('.views-field-title,.views-field-field-address,.views-field-field-phone, .views-field-field-vremya-raboti, .views-field-body').appendTo($(this).find('.wrapper-t'));


        $(this).find('.wrapper-t').append(new_elem);
        number = $(this).find('.views-field-field-phone .services-phone').text();
        $(this).find('.buttons-call-to-action a').attr('href','tel:'+number);
        $(this).find('.buttons-call-to-action .buttons-c-t-a-map').attr('data-adress',index);
    });



    //$('.hotels-and-services .services-item').each(function(index){
    //    $(this).append(new_elem);
    //    number = $(this).find('.views-field-field-phone .services-phone').text();
    //   $(this).find('.buttons-call-to-action a').attr('href','tel:'+number);
    //   $(this).find('.buttons-call-to-action .buttons-c-t-a-map').attr('data-adress',index);
    //
    //});

//    Построение яндекс карты в модальных окнах


    var adress_to_map,index_data;
    var address;
    var feature_geo;

    $('body').on('click','.buttons-c-t-a-map',function(){//Открываю модально окно по клику

        index_data = $(this).attr('data-adress');//получаю значение data для получения координат из массива
        adress_to_map = $(this).parents('.services-item').find('.wrapper-t .views-field-field-address .services-address').text();//получаю адрес

        address = 'Малоярославец'+adress_to_map; //адресс пункта, координаты, кторого надо узнать

        $.ajax({
            method: 'GET',
            url: 'https://geocode-maps.yandex.ru/1.x/?geocode='+address+'&format=json',
            success: function(json){
                var feature = json.response.GeoObjectCollection.featureMember[0];
                feature_geo = feature.GeoObject.Point.pos.split(' ').reverse();
                if (!feature) console.log('Не удалось распознать адрес'); else console.log(feature_geo);
                ya_map(feature_geo, adress_to_map);//пердаю координаты
            }
        });


//        ya_map(index_data, adress_to_map);//передаю в функцию индекс для массива с координатами


        $('.parent').css('display','flex');
        $('#lean_overlay').show();//фон затемнение
    });


    var massive_map = [[55.012187,36.472265],[55.011959,36.457569],[55.002175,36.439836],[54.999356,36.440052]];


    var myMap;
    function ya_map(index,adress){
        ymaps.ready(function () {
            myMap = new ymaps.Map('block', {
//                        center: massive_map[0],// координаты передаются из массива
                center: index,//координаты передаются на прямую
                zoom: 16
            }, {
                searchControlProvider: 'yandex#search'
            });
            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                hintContent: adress
            });

            myMap.geoObjects.add(myPlacemark);
        });
    }

    $('.close-menu').on('click',function(){//Закрываю модальное окно по клику на крестик
        $('.parent').css('display','none');
        $('#lean_overlay').hide();
        myMap.destroy();//убиваю карту

    });

    $('.parent').on('click', function (e) {//Закрываю модальное окно по клику рабочей области
        $('.parent').css('display', 'none');
        $("#lean_overlay").hide();
        myMap.destroy();
    }).on('click', '#block', function (e) {//Запрещаю закрывать окно по клику на само молдальное окно
        e.stopPropagation();

    });




//        var address = 'Малоярославец, ул. Пролетарская, д.3-а';
//         $.ajax({
//            method: 'GET',
//            url: 'https://geocode-maps.yandex.ru/1.x/?geocode='+address+'&format=json',
//             success: function(json){
//                 var feature = json.response.GeoObjectCollection.featureMember[0];
//                 feature_geo = '['+feature.GeoObject.Point.pos.split(' ').reverse().join(', ')+']';
//                 if (!feature) console.log('Не удалось распознать адрес'); else console.log(feature_geo);
//             }
//        });

});
