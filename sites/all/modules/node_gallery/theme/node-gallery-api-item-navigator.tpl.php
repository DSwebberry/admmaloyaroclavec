<?php

/**
 * @file
 * Default theme implementation to show a 6 column table containing the image navigator.
 *
 * Available variables:
 * @todo: fill this in
 *
 * @see theme_preprocess_node_gallery_api_item_navigator()
 */
?>
<div id="node-gallery-item-navigator" class="item-navigator foto-gallery-navigator">
  <div>
    <div class="odd navigator-flex">
      <div class="gallery-item-navigator item-navigator-position">
        <?php print t("%current of %total", array('%current' => $navigator['current'], '%total' => $navigator['total'])); ?>
      </div>
      <div class="gallery-item-navigator item-navigator-first">
        <?php print isset($first_link) ? '<a href="'. $first_link .'"><< </a>' : '<<'; ?>
      </div>
      <div class="gallery-item-navigator item-navigator-prev">
        <?php print isset($prev_link) ? '<a href="'. $prev_link .'">< </a>' : '<'; ?>
      </div>
      <div class="gallery-item-navigator item-navigator-next">
        <?php print isset($next_link) ? '<a href="'. $next_link .'"> ></a>' : '>'; ?>
      </div>
      <div class="gallery-item-navigator item-navigator-last">
        <?php print isset($last_link) ? '<a href="'. $last_link .'"> >></a>' : '>>'; ?>
      </div>
      <div class="gallery-item-navigator item-navigator-gallery-link">
        <?php print '<a href="'. $gallery_link .'">'. t("Back to gallery") .'</a>'; ?>
      </div>
    </div>
  </div>
</div>
