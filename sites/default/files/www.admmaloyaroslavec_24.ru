﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">

<head>
<title>Официальный сайт Администрации МО ГП "Города Малоярославец"</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="Официальный сайт Администрации города Малоярославца"/>
<meta name="keywords" content="официальный, сайт, малоярославец, администрация"/>
<meta http-equiv="expires" content="0"/> 
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<link rel="stylesheet" type="text/css" href="/include/styles.css"/>

<link rel="shortcut icon" href="favicon.ico" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
    
<script type="text/javascript" src="/include/js/jquery.js"></script>



<style type="text/css"> img, div, input { behavior: url("/include/slideshow/iepngfix.htc") } </style>
<script type="text/javascript" src="/include/slideshow/init-banner.js"></script>


<script language="javascript" type="text/javascript">
// <!CDATA[

function isWhiteSymbol(symb)
{
  var ret = false;
  switch(symb)
  {
    case " ":
    case "":
    case"\n":
    case "\r":
      ret = true;
    default:   
  }
 return ret;
}

function Button1_onclick() {

var sometext = document.getElementById("TextArea1").value;
var somelen =sometext.length.toString(10);

var wordcount = 0;
var colnotwhite = 0;

var wordbegin = false;

for(var i=0;i < somelen; i++)
{

 
  if( !isWhiteSymbol(sometext.charAt(i)) )
  {
    colnotwhite = colnotwhite + 1;
    if(wordbegin == false)
     {
       wordcount= wordcount+1;
       wordbegin = true;
     }    
  }
 else
  {   
      wordbegin = false;
  } // end if(sometext.charAt(i) != ' ')  
}//end for
alert("Symbols = " + somelen + "\nSymbols in words = " + colnotwhite.toString(10) + "\nWords = " + wordcount.toString(10));

}//end function

// ]]>


$(document).ready(function() {
    $(".progolosovat").click(function(){
        var jthis = this;
        $.ajax({
                type: "POST",
                url: "/include/ajax.php",
                data: {
                    action: "addvote",
                    id_answer:  $('#mainform-' + jthis.id + ' input[name=answer]:checked').val(),
                    id_question: jthis.id
                },
                success:function(data){
                    var summ = $("#vote-summ-" + jthis.id).html();
                    $("#vote-summ-" + jthis.id).html(+summ+1);
                    var obj = jQuery.parseJSON( data );
                    var forming = '<ul style="margin: 0 0 0 0; padding: 0 0 0 20px">';
                    obj.forEach(function(type) {
                        forming += '\
                            <li style="width: 100%; margin: 0; padding: 10px 0 0 0; list-style-type: none;">\
                                <label class="answer">' + type.answer + ':</label>\
                                <label name="result" id="result' + type.id + '" class="result" style="margin: 0 0 3px 10px">' + type.votes + '</label>\
                                <div style="background: #f00; width: ' + type.pct + '%; height: 10px; margin: 3px 0 0 0"></div>\
                            </li>';
                    });
                    forming += '</ul>';
                    $("#mainform-" + jthis.id).html(forming);
                    $("#golosovat-" + jthis.id).css("display", "none");
                }
            });

        });
        
    $(".questions").click(function(){
        var realId = this.id;
        $.ajax({
                type: "POST",
                url: "/include/ajax.php",
                data: {
                    action: "infoquestion",
                    id_question: realId
                },
                success:function(data){
                    var obj = jQuery.parseJSON( data );
                    
                    console.log(obj);
                    
                    
                    var forming = '<ul style="margin: 0 0 0 0; padding: 0 0 10px 20px">';
                    obj.ans.forEach(function(type) {
                        forming += '\
                            <li style="width: 100%; margin: 0; padding: 10px 0 0 0; list-style-type: none;">\
                                <label class="answer">' + type.answer + ':</label>\
                                <label name="result" id="result' + type.id + '" class="result" style="margin: 0 0 3px 10px">' + type.votes + '</label>\
                                <div style="background: #f00; width: ' + type.pct + '%; height: 10px; margin: 3px 0 0 0"></div>\
                            </li>';
                    });
                    forming += '</ul>\
                        <div class="answ-title" style="margin: 20px 0 20px 20px; font-weight: bold">Все голосов: <span class="vote-summ">' + obj.summ + '</span></div>\
                        <div style="height: 1px; background-color:#000065; margin: 0 0 20px 0"></div>';
                    $("#result_" + realId).html(forming);
                }
            });

        });
        
                
});

</script>

<meta 
	name="sputnik-verification" 
	content="VGOAan4JmGCuZzJG"
/>

</head><body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="26px" valign="top"><div style="font-size:12px; padding: 8px 0 0 23px; width: 300px">
  			 <a href="/" title="На главную" style="color:#000000; font-size: 10px">На главную</a>
             				&nbsp;|&nbsp;
             
  			 <a href="/sitemap" title="Карта сайта" style="color:#000000; font-size: 10px">Карта сайта</a>
             				&nbsp;|&nbsp;
             
  			 <a href="/izbrannoe" title="Сделать стартовой" style="color:#000000; font-size: 10px">Сделать стартовой</a>
             
</div></td>
    <td rowspan="2" valign="bottom" background="/images/item_line.gif">


<div style="width: 790px; padding-right: 4px;">
<div style="float:left; padding: 0 21px 0 0">
<table width="107px" border="0" cellspacing="0" cellpadding="0" height="107px">
  <tr>
    <td height="26px" bgcolor="#000065" style="color:#FFF; font-family:Verdana, Geneva, sans-serif; font-size: 12px; text-decoration:none">
        <a href="/info" title="Официально" style="color:#FFF; text-decoration:none; font-size: 10px">
        	<div align="center" style="padding-top: 3px">Официально</div>
        </a>
    </td>
  </tr>
  <tr>
    <td width="107px" height="89px">
       
            <div style="position:absolute; top: 26px; z-index: 20"> <a href="/info" title="Официально"><img src="/images/item_menu.gif" border="0" /></a></div>
            <div style="position:absolute; top: 37px; padding-left: 3px; z-index: 10" align="center"><img src="/images/0.jpg" border="0" /></div>
        
</td>
  </tr>
</table>


</div>
<div style="float:left; padding: 0 21px 0 0">
<table width="107px" border="0" cellspacing="0" cellpadding="0" height="107px">
  <tr>
    <td height="26px" bgcolor="#000065" style="color:#FFF; font-family:Verdana, Geneva, sans-serif; font-size: 12px; text-decoration:none">
        <a href="/fotoalbum" title="Фотогалерея" style="color:#FFF; text-decoration:none; font-size: 10px">
        	<div align="center" style="padding-top: 3px">Фотогалерея</div>
        </a>
    </td>
  </tr>
  <tr>
    <td width="107px" height="89px">
       
            <div style="position:absolute; top: 26px; z-index: 20"> <a href="/fotoalbum" title="Фотогалерея"><img src="/images/item_menu.gif" border="0" /></a></div>
            <div style="position:absolute; top: 37px; padding-left: 3px; z-index: 10" align="center"><img src="/images/1.jpg" border="0" /></div>
        
</td>
  </tr>
</table>


</div>
<div style="float:left; padding: 0 21px 0 0">
<table width="107px" border="0" cellspacing="0" cellpadding="0" height="107px">
  <tr>
    <td height="26px" bgcolor="#000065" style="color:#FFF; font-family:Verdana, Geneva, sans-serif; font-size: 12px; text-decoration:none">
        <a href="http://www.admmaloyaroslavec.ru/komf_gor_sreda" title="Городская среда" style="color:#FFF; text-decoration:none; font-size: 10px">
        	<div align="center" style="padding-top: 3px">Городская среда</div>
        </a>
    </td>
  </tr>
  <tr>
    <td width="107px" height="89px">
       
            <div style="position:absolute; top: 26px; z-index: 20"> <a href="http://www.admmaloyaroslavec.ru/komf_gor_sreda" title="Городская среда"><img src="/images/item_menu.gif" border="0" /></a></div>
            <div style="position:absolute; top: 37px; padding-left: 3px; z-index: 10" align="center"><img src="/images/2.jpg" border="0" /></div>
        
</td>
  </tr>
</table>


</div>
<div style="float:left; padding: 0 21px 0 0">
<table width="107px" border="0" cellspacing="0" cellpadding="0" height="107px">
  <tr>
    <td height="26px" bgcolor="#000065" style="color:#FFF; font-family:Verdana, Geneva, sans-serif; font-size: 12px; text-decoration:none">
        <a href="prezident_rf" title="Президент России" style="color:#FFF; text-decoration:none; font-size: 10px">
        	<div align="center" style="padding-top: 3px">Президент России</div>
        </a>
    </td>
  </tr>
  <tr>
    <td width="107px" height="89px">
       
            <div style="position:absolute; top: 26px; z-index: 20"> <a href="prezident_rf" title="Президент России"><img src="/images/item_menu.gif" border="0" /></a></div>
            <div style="position:absolute; top: 37px; padding-left: 3px; z-index: 10" align="center"><img src="/images/3.jpg" border="0" /></div>
        
</td>
  </tr>
</table>


</div>
<div style="float:left; padding: 0 21px 0 0">
<table width="107px" border="0" cellspacing="0" cellpadding="0" height="107px">
  <tr>
    <td height="26px" bgcolor="#000065" style="color:#FFF; font-family:Verdana, Geneva, sans-serif; font-size: 12px; text-decoration:none">
        <a href="/kontacts" title="Контакты" style="color:#FFF; text-decoration:none; font-size: 10px">
        	<div align="center" style="padding-top: 3px">Контакты</div>
        </a>
    </td>
  </tr>
  <tr>
    <td width="107px" height="89px">
       
            <div style="position:absolute; top: 26px; z-index: 20"> <a href="/kontacts" title="Контакты"><img src="/images/item_menu.gif" border="0" /></a></div>
            <div style="position:absolute; top: 37px; padding-left: 3px; z-index: 10" align="center"><img src="/images/4.jpg" border="0" /></div>
        
</td>
  </tr>
</table>


</div>
<div style="float:left; padding: 0 21px 0 0">
<table width="107px" border="0" cellspacing="0" cellpadding="0" height="107px">
  <tr>
    <td height="26px" bgcolor="#000065" style="color:#FFF; font-family:Verdana, Geneva, sans-serif; font-size: 12px; text-decoration:none">
        <a href="/daty" title="Памятные даты" style="color:#FFF; text-decoration:none; font-size: 10px">
        	<div align="center" style="padding-top: 3px">Памятные даты</div>
        </a>
    </td>
  </tr>
  <tr>
    <td width="107px" height="89px">
       
            <div style="position:absolute; top: 26px; z-index: 20"> <a href="/daty" title="Памятные даты"><img src="/images/item_menu.gif" border="0" /></a></div>
            <div style="position:absolute; top: 37px; padding-left: 3px; z-index: 10" align="center"><img src="/images/5.jpg" border="0" /></div>
        
</td>
  </tr>
</table>


</div>

</div></td>
  </tr>
  <tr>
    <td height="90px" width="100%"  bgcolor="#000065"><div style="margin: 0 0 0 48px"><img src="/images/main_2.jpg" border=0 /></div></td>
  </tr>
</table>
<div style="background-image:url(/images/top_fon_line.jpg)">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:url(/images/top_fon.jpg); background-repeat:no-repeat;  " >
  <tr>
    <td width="683px" height="387px"><div class="osg">Официальный сайт города</div><img src="/images/main_1.jpg" border=0 /></td>
    <td height="387px" ><div class="svitok">    <div class="dates_zag">События и даты</div>

        <div class="dates">» 23 июля - В этот день в 1240 году русские воины под командованием князя Александра Ярославича одержали победу над шведами в Невской битве.</div>
        <div class="dates">» 18 июля - В этот день в 1770 год русская армия под командованием Петра Александровича Румянцева одержала победу над турецкой армией при Ларге.</div>
        <div class="dates">» 15 июля - В этот день в 1410 году русские войска и их союзники одержали победу над немецкими рыцарями в Грюнвальдской битве.</div>
        <div class="dates">» 13 июля - Освобождение Вильнюса от немецко-фашистских захватчиков, 1944г.</div>
        <div class="dates">» 12 июля в 1943 году под Прохоровкой произошло крупнейшее во Второй мировой войне танковое сражение между советской и германской армиями.</div>
        <div class="dates">» 10 июля - День воинской славы России. В 1709 году русская армия под командованием Петра Первого одержала победу над шведскими войсками в Полтавском сражении.</div>
        <div class="dates">» 7 июля - День воинской славы России. Победа в Чесменском сражении, 1770г.</div>
        <div class="dates">» 5 июля 1943 года началось одно из ключевых сражений Великой Отечественной войны –Курская битва.</div>
<div align="right">
<a href="/dates" class="links">Полный список событий...</a>
</div>
</div><div align="right" style="padding: 0 20px 0 0"><img src="/images/svitok.png" border=0 /></div></td>
  </tr>
  <tr>
    <td height="19px" colspan="2" background="/images/line_2.jpg"></td>
  </tr>
</table>
</div>
<div align="center">
<table width="1253px" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top"><div style="padding: 0 0px 0 19px">
            	        <div style=" width: 228px; float:left; padding: 0 20px 0 0">
                
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
  <tr>
    <td height="300px" valign="top" bgcolor="#f3f3f3">
            <div class="menu_main" style="padding: 7px 10px 0px 10px">ВЛАСТЬ</div>
                                        <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/adm_gor" class="menu_main" title="Администрация города">Администрация города</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/gor_duma" class="menu_main" title="Городская Дума">Городская Дума</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/pravovie_akty" class="menu_main" title="Муниципальные правовые акты">Муниципальные правовые акты</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/gos_uslugy" class="menu_main" title="Муниципальные услуги">Муниципальные услуги</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/slushaniya" class="menu_main" title="Публичные слушания">Публичные слушания</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/gradostroitelstvo" class="menu_main" title="Градостроительство">Градостроительство</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/mun_control" class="menu_main" title="Муниципальный контроль">Муниципальный контроль</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/personal_dannye" class="menu_main" title="Политика в отношении персональных данных">Политика в отношении персональных данных</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/k_s_k" class="menu_main" title="Контрольно-счётная комиссия">Контрольно-счётная комиссия</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/izb_kom" class="menu_main" title="Избирательная комиссия">Избирательная комиссия</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/protivodeystvie_korrupcii" class="menu_main" title="Противодействие коррупции">Противодействие коррупции</a></div></div>        
                            
    </td>
  </tr>
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
</table>
       
            
            
        </div>
            	        <div style=" width: 228px; float:left; padding: 0 20px 0 0">
                
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
  <tr>
    <td height="300px" valign="top" bgcolor="#f3f3f3">
            <div class="menu_main" style="padding: 7px 10px 0px 10px">НАШ ГОРОД</div>
                                        <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/history" class="menu_main" title="Историческая  справка">Историческая  справка</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/regalii" class="menu_main" title="Герб, флаг и гимн города">Герб, флаг и гимн города</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/infra" class="menu_main" title="Городская инфраструктура">Городская инфраструктура</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/poch_grazdane" class="menu_main" title="Почётные граждане">Почётные граждане</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/pobratimy" class="menu_main" title="Города-побратимы">Города-побратимы</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/obrazovanie" class="menu_main" title="Образование">Образование</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/kultura" class="menu_main" title="Культура и спорт">Культура и спорт</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/ludy" class="menu_main" title="Знаменитые люди города">Знаменитые люди города</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/gorafisha" class="menu_main" title="Городская афиша">Городская афиша</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/files/sobitia/digest_28_29_07_2018.pdf" class="menu_main" title="Дайджест выходного дня">Дайджест выходного дня</a></div></div>       
                            
    </td>
  </tr>
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
</table>
       
            
            
        </div>
            	        <div style=" width: 228px; float:left; padding: 0 20px 0 0">
                
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
  <tr>
    <td height="300px" valign="top" bgcolor="#f3f3f3">
            <div class="menu_main" style="padding: 7px 10px 0px 10px">ЭКОНОМИКА и  БЛАГОУСТРОЙСТВО</div>
                                        <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/razvitie" class="menu_main" title="Социально-экономическое развитие">Социально-экономическое развитие</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/municipal" class="menu_main" title="Муниципальное   имущество">Муниципальное   имущество</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/budzet" class="menu_main" title="Бюджет">Бюджет</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/mun_zakaz" class="menu_main" title="Муниципальный заказ">Муниципальный заказ</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/ekologiy" class="menu_main" title="Благоустройство и  экология">Благоустройство и  экология</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/zakupki" class="menu_main" title="Положение о закупках">Положение о закупках</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/mun_prog" class="menu_main" title="Муниципальные программы">Муниципальные программы</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/torgovlya" class="menu_main" title="Торговля">Торговля</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/pkr_2017" class="menu_main" title="Программы комплексного развития">Программы комплексного развития</a></div></div>        
                            
    </td>
  </tr>
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
</table>
       
            
            
        </div>
            	        <div style=" width: 228px; float:left; padding: 0 20px 0 0">
                
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
  <tr>
    <td height="300px" valign="top" bgcolor="#f3f3f3">
            <div class="menu_main" style="padding: 7px 10px 0px 10px">ИНФОРМАЦИЯ</div>
                                        <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/uchr_goroda" class="menu_main" title="Предприятия  и учреждения города">Предприятия  и учреждения города</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/inf_gkh" class="menu_main" title="Информация ЖКХ">Информация ЖКХ</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/smy" class="menu_main" title="СМИ города">СМИ города</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/tel_ex_sluzby" class="menu_main" title="Телефоны экстренных служб">Телефоны экстренных служб</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/organizacii" class="menu_main" title="Некоммерческие организации">Некоммерческие организации</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/gor_transport" class="menu_main" title="Городской транспорт">Городской транспорт</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/komf_gor_sreda" class="menu_main" title="Формирование современной городской среды">Формирование современной городской среды</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/transp_prokuratura" class="menu_main" title="Калужская транспортная прокуратура">Калужская транспортная прокуратура</a></div></div>        
                            
    </td>
  </tr>
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
</table>
       
            
            
        </div>
            	        <div style=" width: 228px; float:left; padding: 0 0px 0 0">
                
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
  <tr>
    <td height="300px" valign="top" bgcolor="#f3f3f3">
            <div class="menu_main" style="padding: 7px 10px 0px 10px">ТУРИЗМ</div>
                                        <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/pamiatniki" class="menu_main" title="Памятники истории и культуры">Памятники истории и культуры</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/tur_marsh" class="menu_main" title="Туристические маршруты">Туристические маршруты</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/map" class="menu_main" title="Карта города">Карта города</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/muzey" class="menu_main" title="Музеи и выставочные центры">Музеи и выставочные центры</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/servis" class="menu_main" title="Сервис (гостиницы, рестораны, кафе)">Сервис (гостиницы, рестораны, кафе)</a></div></div>        
                                                    <div style=" padding: 10px 10px 0 10px;"><div style="width: 10px; position:absolute;">-</div> <div style="padding: 2px 0 0 12px"><a href="/dosug" class="menu_main" title="Объекты досуга">Объекты досуга</a></div></div>        
                            
    </td>
  </tr>
  <tr>
    <td><div style=" height:1px; background-color:#000065"></div></td>
  </tr>
</table>
       
            
            
        </div>
    </div></td>
  </tr>
</table>
</div>
<div style=" height:6px;"></div>
<div align="center">

<table width="1253px" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="19px">&nbsp;</td>
    <td width="476px" valign="top"><div style="height: 16px; background-color:#000065"></div>
<div style="padding: 0px 0px 0 0px; ">
<div class="menu_main" style="padding: 12px 10px 0px 0px">НОВОСТИ</div>

<div style="height: 15px"></div>

<div style="text-decoration:none; color: #717171; font-size: 9px;">26.07.2018 10:53</div>
<a href="/news/1259"><div style="font-size:11px; padding: 0px 0px 3px 0; line-height: 15px; color:#ce240e">Календарь памятных дат военной истории России на август...</div></a>
<a href="/news/1259"><div style="font-size:11px; padding: 0px 0px 3px 0; line-height: 15px; color:#000000""> 




1 августа


День памяти о&nbsp;погибших в&nbsp;Первой мировой войне. В&nbsp;этот день в&nbsp;1914 году Германия объявила войну России.
Подробнее&hellip;
Видеоролик:
YouTube&nbsp;&nbsp;Яндекс




2 августа


В&nbspэтот...</div></a>
<div align="right"><a href="/news/1259" class="links">Подробнее...</a></div>

<div style="height: 15px"></div>

<div style="text-decoration:none; color: #717171; font-size: 9px;">25.07.2018 11:42</div>
<a href="/news/1268"><div style="font-size:11px; padding: 0px 0px 3px 0; line-height: 15px; color:#ce240e">В Калужской области обсудили актуальные вопросы организации муниципального земельного контроля...</div></a>
<a href="/news/1268"><div style="font-size:11px; padding: 0px 0px 3px 0; line-height: 15px; color:#000000""> 24 июля в&nbsp;Калуге министр сельского хозяйства области Леонид Громов принял участие в&nbsp;семинаре-совещании органов местного самоуправления, осуществляющих муниципальный земельный...</div></a>
<div align="right"><a href="/news/1268" class="links">Подробнее...</a></div>

<div style="height: 15px"></div>

<div style="text-decoration:none; color: #717171; font-size: 9px;">25.07.2018 10:11</div>
<a href="/news/1267"><div style="font-size:11px; padding: 0px 0px 3px 0; line-height: 15px; color:#ce240e">Анатолий Артамонов принял участие в селекторном совещании Министра МЧС России...</div></a>
<a href="/news/1267"><div style="font-size:11px; padding: 0px 0px 3px 0; line-height: 15px; color:#000000""> 24 июля в&nbsp;Москве Министр РФ&nbsp;по&nbsp;делам гражданской обороны, чрезвычайным ситуациям и&nbsp;ликвидации последствий стихийных бедствий Евгений Зиничев провел селекторное совещание...</div></a>
<div align="right"><a href="/news/1267" class="links">Подробнее...</a></div>


<div style="padding: 7px 0px 8px 0px"><a href="/news" style="text-decoration:none" class="links">Все материалы раздела...</a></div>


</div>﻿<div style="height: 16px; background-color:#000065"></div><section class="golosovanie"><h3 class='menu_main'>ОПРОС</h3><div class="answ-title" style="font-weight: bold; font-size: 14px; color: #f00;">Готовы ли Вы сортировать мусор?</div><div class="answ-title" style="font-weight: bold; margin: 0 0 10px 0">Информация по теме опроса: <a href="http://www.admmaloyaroslavec.ru/news/1182">http://www.admmaloyaroslavec.ru/news/1182</a></div><div class="answ-title" style="margin: 3px 0 0 0">Всего голосов: <span id="vote-summ-75">81</span></div><form method="post" action="" class='mainform' id='mainform-75'><ul style="margin: 0 0 0 0; padding: 0 0 0 20px"><li style="width: 100%; margin: 0; padding: 10px 0 0 0; list-style-type: none;"><input type="radio" name="answer" value="100" checked/><label class="answer">да</label><label name="result" id="result100" class="result" style="display: none;">71</label></li><li style="width: 100%; margin: 0; padding: 10px 0 0 0; list-style-type: none;"><input type="radio" name="answer" value="101" /><label class="answer">нет</label><label name="result" id="result101" class="result" style="display: none;">9</label></li><li style="width: 100%; margin: 0; padding: 10px 0 0 0; list-style-type: none;"><input type="radio" name="answer" value="102" /><label class="answer">затрудняюсь ответить</label><label name="result" id="result102" class="result" style="display: none;">1</label></li></ul></form><div class="golosovat" id="golosovat-75" style='margin: 15px 0 0 0'><input type="button" value="Голосовать" class="progolosovat" id='75'/></div><div style="margin: 10px 0 0 0"><a href="/opros" class="links"/>Еще информации по теме...  </a></div></section><div class="razdel_20"></div><div class="line-grey"></div></td>
    <td width="20px">&nbsp;</td>
    <td width="228px" valign="top"><div style="height: 16px; background-color:#000065"></div>
<a href="/fotoalbum"><div class="menu_main" style="padding: 12px 10px 15px 0px">ФОТОАЛЬБОМ</div></a>
<a href="/fotoalbum"><img src="/images/fotoalbum.jpg" border="0" /></a>

<div style="height: 20px;"></div>﻿<div style="height: 16px; background-color:#000065"></div>
<a href="prezident_rf"><div class="menu_main" style="padding: 12px 10px 15px 0px"></div></a>
<a href="prezident_rf"><img src="/images/bill.jpg" border="0" /></a>
</td>
    <td width="20px">&nbsp;</td>
    <td width="476px"  valign="top"><div style="height: 16px; background-color:#000065"></div>
<div style="padding: 0px 0px 15px 0px; ">
<div class="menu_main" style="padding: 12px 10px 0px 0px">ОФИЦИАЛЬНАЯ ИНФОРМАЦИЯ</div>

<div style="height: 15px"></div>

<a href="/info/10577"><div style="float:left; width: 10px; position:absolute; color:#ce240e; font-size: 14px"><b>!</b></div><div style="font-size:11px; padding: 2px 0px 3px 12px; line-height: 15px; color:#000000"> ИНФОРМАЦИОННОЕ СООБЩЕНИЕ
о&nbsp;приеме заявлений о&nbsp;предоставлении в&nbsp;аренду земельного участка для...</div></a>
<div align="right"><a href="/info/10577" class="links">Подробнее...</a></div>

<div style="height: 15px"></div>

<a href="/info/10576"><div style="float:left; width: 10px; position:absolute; color:#ce240e; font-size: 14px"><b>!</b></div><div style="font-size:11px; padding: 2px 0px 3px 12px; line-height: 15px; color:#000000"> Фонд имущества Калужской области сообщает об&nbsp;итогах объявленной на&nbsp;2 июля 2018 г. продажи муниципального...</div></a>
<div align="right"><a href="/info/10576" class="links">Подробнее...</a></div>

<div style="height: 15px"></div>

<a href="/info/10575"><div style="float:left; width: 10px; position:absolute; color:#ce240e; font-size: 14px"><b>!</b></div><div style="font-size:11px; padding: 2px 0px 3px 12px; line-height: 15px; color:#000000"> ОБЪЯВЛЕНИЕ
о&nbsp;проведении конкурса на&nbsp;предоставление субсидий некоммерческим организациям...</div></a>
<div align="right"><a href="/info/10575" class="links">Подробнее...</a></div>


<div style="padding: 7px 0px 8px 0px"><a href="/info" style="text-decoration:none" class="links">Все материалы раздела...</a></div>


</div><div style="height: 16px; background-color:#000065"></div>
<div style="padding: 0px 0px 15px 0px; ">
<div class="menu_main" style="padding: 12px 10px 0px 0px">ПОЛЕЗНЫЕ ССЫЛКИ</div>
<div style="padding: 5px 20px 0 0;">
    				<div style="padding: 7px 20px 0 0;"><div style="float:left; width: 10px; position:absolute; color:#ce240e; font-size: 14px"><b>»</b></div><a href="http://www.admoblkaluga.ru/main/" class="menu_bottom" target="_blank" title="Портал органов власти Калужской области"><div style="padding: 2px 0px 3px 20px;">Портал органов власти Калужской области</div></a>        
         </div>
    				<div style="padding: 7px 20px 0 0;"><div style="float:left; width: 10px; position:absolute; color:#ce240e; font-size: 14px"><b>»</b></div><a href="https://www.gosuslugi.ru/" class="menu_bottom" target="_blank" title="Портал государственных услуг"><div style="padding: 2px 0px 3px 20px;">Портал государственных услуг</div></a>        
         </div>
    				<div style="padding: 7px 20px 0 0;"><div style="float:left; width: 10px; position:absolute; color:#ce240e; font-size: 14px"><b>»</b></div><a href="http://maloyar.ru/" class="menu_bottom" target="_blank" title="Официальный сайт Малоярославецкой районной администрации"><div style="padding: 2px 0px 3px 20px;">Официальный сайт Малоярославецкой районной администрации</div></a>        
         </div>
    				<div style="padding: 7px 20px 0 0;"><div style="float:left; width: 10px; position:absolute; color:#ce240e; font-size: 14px"><b>»</b></div><a href="http://gov.ru/" class="menu_bottom" target="_blank" title="Сервер органов государственной власти РФ"><div style="padding: 2px 0px 3px 20px;">Сервер органов государственной власти РФ</div></a>        
         </div>
</div>
</div></td>
    <td width="14px">&nbsp;</td>
  </tr>
</table>
</div>
<div style=" height:15px;"></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
    <td height="19px" background="/images/line_2.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td height="50px"><div align="center"><div style="width:1253px; height:50px;"><div style="float:left; width:880px; height: 80px;">
    <div style="padding: 7px 0 0 25px;" align="left" class="footer">
    © 2010-2018 Администрация МО ГП "Город Малоярославец"

    <script type="text/javascript">
       (function(d, t, p) {
           var j = d.createElement(t); j.async = true; j.type = "text/javascript";
           j.src = ("https:" == p ? "https:" : "http:") + "//stat.sputnik.ru/cnt.js";
           var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);
       })(document, "script", document.location.protocol);
    </script>
    </div>
    <div style=" padding: 7px 0 0 25px;" align="left" class="footer">
    249096,  Калужская обл.,  г.Малоярославец,  ул.Калужская, д.7, тел.: (48431) 3-11-25,  факс: 2-14-36, e-mail: <a class="footer" href="mailto:meria@kaluga.ru, a.otdel@inbox.ru">meria@kaluga.ru, a.otdel@inbox.ru</a>    </div>
    <div style="padding: 7px 20px 0 25px;" align="left" class="footer">
    Дизайн и программирование: <a class="footer" href="mailto:sitestroy@criticworld.ru">sitestroy@criticworld.ru</a>
    </div>
</div>


<div style="float: left; width: 350px; padding: 20px 0 0 10px;" align="right" class="footer">
<!--Rating@Mail.ru counter-->
<script language="javascript"><!--
d=document;var a='';a+=';r='+escape(d.referrer);js=10;//--></script>
<script language="javascript1.1"><!--
a+=';j='+navigator.javaEnabled();js=11;//--></script>
<script language="javascript1.2"><!--
s=screen;a+=';s='+s.width+'*'+s.height;
a+=';d='+(s.colorDepth?s.colorDepth:s.pixelDepth);js=12;//--></script>
<script language="javascript1.3"><!--
js=13;//--></script><script language="javascript" type="text/javascript"><!--
d.write('<a href="http://top.mail.ru/jump?from=2166633" target="_top">'+
'<img src="http://df.c0.b1.a2.top.mail.ru/counter?id=2166633;t=211;js='+js+
a+';rand='+Math.random()+'" alt="Рейтинг@Mail.ru" border="0" '+
'height="31" width="88"></a>');if(11<js)d.write('<'+'!-- ');//--></script>
<noscript><a target="_top" href="http://top.mail.ru/jump?from=2166633">
<img src="http://df.c0.b1.a2.top.mail.ru/counter?js=na;id=2166633;t=211" 
height="31" width="88" border="0" alt="Рейтинг@Mail.ru"></a></noscript>
<script language="javascript" type="text/javascript"><!--
if(11<js)d.write('--'+'>');//--></script>
<!--// Rating@Mail.ru counter-->

<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t15.1;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число просмотров за 24"+
" часа, посетителей за 24 часа и за сегодня' "+
"border='0' width='88' height='31'></a>")
//--></script><!--/LiveInternet--><span id="sputnik-informer"></span>

</div></div></div></td>
  </tr>
</table>




</body>
</html>